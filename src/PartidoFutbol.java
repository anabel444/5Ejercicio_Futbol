//CLASE PARTIDOS DE FUTBOL
public class PartidoFutbol {
	private String equipoLocal;
	private String equipoVisita;
	private int golesLocal;
	private int golesVisita;
	
	@Override
	public String toString() {
		return "PartidoFutbol [equipoLocal=" + equipoLocal + ", equipoVisita=" + equipoVisita + ", golesLocal="
				+ golesLocal + ", golesVisita=" + golesVisita + "]";
	}

	public PartidoFutbol(String equipoLocal, String equipoVisita, int golesLocal, int golesVisita) {
		super();
		this.equipoLocal = equipoLocal;
		this.equipoVisita = equipoVisita;
		this.golesLocal = golesLocal;
		this.golesVisita = golesVisita;
	}

	public PartidoFutbol() {
		super();
	}

	public String getEquipoLocal() {
		return equipoLocal;
	}

	public void setEquipoLocal(String equipoLocal) {
		this.equipoLocal = equipoLocal;
	}

	public String getEquipoVisita() {
		return equipoVisita;
	}

	public void setEquipoVisita(String equipoVisita) {
		this.equipoVisita = equipoVisita;
	}

	public int getGolesLocal() {
		return golesLocal;
	}

	public void setGolesLocal(int golesLocal) {
		this.golesLocal = golesLocal;
	}

	public int getGolesVisita() {
		return golesVisita;
	}

	public void setGolesVisita(int golesVisita) {
		this.golesVisita = golesVisita;
	}
	
	
	
	
}
