//principal reference origins4

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		
		ArrayList<PartidoFutbol> partidos=new ArrayList<PartidoFutbol>();
		
		
		File fichero=new File("partidos.txt");
		
		try {
			Scanner scan=new Scanner(fichero);
			
			while (scan.hasNextLine()){
			
			String linea=scan.nextLine();
			
			String partes[]=linea.split("::");
			
			//SE PUEDE HACER ASI o de la otra manera
			//PartidoFutbol encuentro=new PartidoFutbol(partes[0],partes[1]...);
			
			PartidoFutbol encuentro=new PartidoFutbol();
			encuentro.setEquipoLocal(partes[0]);
			encuentro.setEquipoVisita(partes[1]);
			encuentro.setGolesLocal(Integer.parseInt(partes[2]));
			encuentro.setGolesVisita(Integer.parseInt(partes[3]));
			
			partidos.add(encuentro);
			}
			// for r�pido PARA CADA PARTIDO EN PARTIDOS
			for ( PartidoFutbol aux: partidos){
				System.out.println(aux.toString());
			}

		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
}
}
	
